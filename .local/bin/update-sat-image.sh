#!/bin/sh

NAME=~/.cache/goes-latest.jpg
SOURCE=https://www.ssec.wisc.edu/data/geo/images/goes-16/latest_goes-16_rgbstr_fd.jpg

curl -q ${SOURCE} -o ${NAME}.tmp || rm -f ${NAME}.tmp
convert ${NAME}.tmp -crop 1100x1090+0+10 -filter Lanczos -resize 60% -gamma 1.2 -quality 100 ${NAME}.tmp || rm -f ${NAME}.tmp

if [ -f ${NAME}.tmp ]; then
    mv -b ${NAME}.tmp ${NAME}
    nitrogen --set-centered ${NAME}
fi
