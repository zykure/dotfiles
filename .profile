export GPG_TTY=$(tty)

#export CC=/usr/bin/gcc CXX=/usr/bin/g++
export CC=/usr/bin/clang CXX=/usr/bin/clang++
#export CC=/usr/lib/ccache/bin/clang CXX=/usr/lib/ccache/bin/clang++

case $- in
  *i*)
    # Interactive session. Try switching to zsh.
    zsh=$(command -v zsh)
    if [ -x "$zsh" ]; then
      export SHELL="$zsh"
      exec "$zsh" -l
    fi
esac
