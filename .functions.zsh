# https://github.com/facundoolano/rpg-cli

RPG_CLI=$HOME/rpg-cli/target/release/rpg-cli
rpg () {
   $RPG_CLI "$@"
   cd "$($RPG_CLI --pwd)"
}
