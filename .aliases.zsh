# https://wiki.archlinux.org/index.php/Dotfiles
alias dotz='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

#alias ls='lsd --color=auto --icon=auto'
#alias ls='exa --color=auto --no-icons'
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'
alias lsdir='ls -d */'

alias exa="exa --long --color-scale --icons"
alias ag="ag --smart-case"

alias trim="awk '{ gsub(/^[ \t]+|[ \t]+$/, \"\"); print }'"

alias ncmake="cmake -G Ninja"
alias nccmake="ccmake -G Ninja"
alias ninja="nice ninja -k1 -j2 -l3"
alias make="nice make -j2 -l3"

alias h5dump="h5dump -m '%.9g'"
alias pdf2png="pdftoppm -singlefile -png"

alias filter-sources="egrep '\.([chi]pp|[chi]xx|[chi]{2}|h|[CHI])$'"

alias tbrowser="root -l -e 'new TBrowser();'"

alias xkbmap="setxkbmap us de_se_fi"
