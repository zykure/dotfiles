# zykure's dotfiles

## Setup:
```
git init --bare ~/.dotfiles
alias dotz='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

dotz config status.showUntrackedFiles no
dotz config pull.rebase true
dotz remote add origin git@gitlab.com:zykure/dotfiles.git
dotz pull origin master
dotz branch --set-upstream-to=origin/master master
```

Then use any git command with the `dotz` alias, e.g. `dotz pull`.


## Dependencies:
```
# ZSH
sudo apt install zsh

# ZI
sh -c "$(curl -fsSL git.io/get-zi)" -- -a annex
```

## References:
* dotfiles:
  * https://mywiki.wooledge.org/DotFiles
  * https://wiki.archlinux.org/index.php/Dotfiles
  * https://news.ycombinator.com/item?id=11070797
* zi:
  * https://wiki.zshell.dev/
