#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

case $- in
  *i*)
    # Interactive session. Try switching to zsh.
    zsh=$(command -v zsh)
    if [ -x "$zsh" ]; then
      export SHELL="$zsh"
      exec "$zsh" -l
    fi
esac
