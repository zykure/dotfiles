# Command to run program, show backtrace, and quit. Useful for debugging in scripts. Generated with:
#   for k in $(seq 0 20); do echo "\tif \$argc == $k"; echo -n "\t\trun "; for i in $(seq 0 $(($k-1))); do echo -n "\$arg$i "; done; echo; echo "\tend"; done
define run_bt_quit
	if $argc == 0
		run 
	end
	if $argc == 1
		run $arg0 
	end
	if $argc == 2
		run $arg0 $arg1 
	end
	if $argc == 3
		run $arg0 $arg1 $arg2 
	end
	if $argc == 4
		run $arg0 $arg1 $arg2 $arg3 
	end
	if $argc == 5
		run $arg0 $arg1 $arg2 $arg3 $arg4 
	end
	if $argc == 6
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 
	end
	if $argc == 7
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 
	end
	if $argc == 8
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 
	end
	if $argc == 9
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 
	end
	if $argc == 10
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 
	end
	if $argc == 11
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 
	end
	if $argc == 12
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 
	end
	if $argc == 13
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 
	end
	if $argc == 14
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 
	end
	if $argc == 15
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 
	end
	if $argc == 16
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 $arg15 
	end
	if $argc == 17
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 $arg15 $arg16 
	end
	if $argc == 18
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 $arg15 $arg16 $arg17 
	end
	if $argc == 19
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 $arg15 $arg16 $arg17 $arg18 
	end
	if $argc == 20
		run $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10 $arg11 $arg12 $arg13 $arg14 $arg15 $arg16 $arg17 $arg18 $arg19 
	end

    finish

    if $ != 0
        bt
    end

    quit
end

set debuginfod enabled on
