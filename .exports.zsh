# Switch terminal color mode in tmux sessions
[[ $TMUX = "" ]] && export TERM="xterm-256color"
[[ $TMUX != "" ]] && export TERM="screen-256color"

export PATH=$HOME/.local/bin:/var/lib/snapd/snap/bin/:$PATH
export PYTHONPATH=$HOME/.local/share/tine/python:$PYTHONPATH

export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

export VISUAL=nano
export EDITOR="$VISUAL"
