#!usr/bin/env zsh

# - - - - - - - - - - - - - - - - - - - -
# Profiling Tools
# - - - - - - - - - - - - - - - - - - - -

PROFILE_STARTUP=false
if [[ "$PROFILE_STARTUP" == true ]]; then
    zmodload zsh/zprof
    # http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html
    PS4=$'%D{%M%S%.} %N:%i> '
    exec 3>&2 2>$HOME/startlog.$$
    setopt xtrace prompt_subst
fi


# - - - - - - - - - - - - - - - - - - - -
# Instant Prompt
# - - - - - - - - - - - - - - - - - - - -

# Enable Powerlevel10k instant prompt. Should stay close to the top of `~/.zshrc`.
# Initialization code that may require console input ( password prompts, [y/n]
# confirmations, etc. ) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


# - - - - - - - - - - - - - - - - - - - -
# Zsh Core Configuration
# - - - - - - - - - - - - - - - - - - - -

# Install Functions.
export XDG_CONFIG_HOME="$HOME/.config"
export UPDATE_INTERVAL=15

# Load The Prompt System And Completion System And Initilize Them.
autoload -Uz compinit promptinit

# Load And Initialize The Completion System Ignoring Insecure Directories With A
# Cache Time Of 20 Hours, So It Should Almost Always Regenerate The First Time A
# Shell Is Opened Each Day.
# See: https://gist.github.com/ctechols/ca1035271ad134841284
_comp_files=(${ZDOTDIR:-$HOME}/.zcompdump(Nm-20))
if (( $#_comp_files )); then
    compinit -i -C
else
    compinit -i
fi
unset _comp_files
promptinit
setopt prompt_subst


# - - - - - - - - - - - - - - - - - - - -
# ZSH Settings
# - - - - - - - - - - - - - - - - - - - -

bindkey    "^[[3~"          delete-char
bindkey    "^[3;5~"         delete-char

autoload -U colors && colors    # Load Colors.
unsetopt case_glob              # Use Case-Insensitve Globbing.
setopt globdots                 # Glob Dotfiles As Well.
setopt extendedglob             # Use Extended Globbing.
setopt autocd                   # Automatically Change Directory If A Directory Is Entered.

# # Smart URLs.
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
#
# # General.
setopt brace_ccl                # Allow Brace Character Class List Expansion.
setopt combining_chars          # Combine Zero-Length Punctuation Characters ( Accents ) With The Base Character.
setopt rc_quotes                # Allow 'Henry''s Garage' instead of 'Henry'\''s Garage'.
unsetopt mail_warning           # Don't Print A Warning Message If A Mail File Has Been Accessed.

# Jobs.
setopt long_list_jobs           # List Jobs In The Long Format By Default.
setopt auto_resume              # Attempt To Resume Existing Job Before Creating A New Process.
setopt notify                   # Report Status Of Background Jobs Immediately.
unsetopt bg_nice                # Don't Run All Background Jobs At A Lower Priority.
unsetopt hup                    # Don't Kill Jobs On Shell Exit.
unsetopt check_jobs             # Don't Report On Jobs When Shell Exit.

unsetopt correct                # Turn Off Corrections

# History.
HISTFILE="${ZDOTDIR:-$HOME}/.zhistory"
HISTSIZE=100000
SAVEHIST=5000
setopt appendhistory notify
unsetopt beep nomatch

setopt bang_hist                # Treat The '!' Character Specially During Expansion.
setopt inc_append_history       # Write To The History File Immediately, Not When The Shell Exits.
setopt share_history            # Share History Between All Sessions.
setopt hist_expire_dups_first   # Expire A Duplicate Event First When Trimming History.
setopt hist_ignore_dups         # Do Not Record An Event That Was Just Recorded Again.
setopt hist_ignore_all_dups     # Delete An Old Recorded Event If A New Event Is A Duplicate.
setopt hist_find_no_dups        # Do Not Display A Previously Found Event.
setopt hist_ignore_space        # Do Not Record An Event Starting With A Space.
setopt hist_save_no_dups        # Do Not Write A Duplicate Event To The History File.
setopt hist_verify              # Do Not Execute Immediately Upon History Expansion.
setopt extended_history         # Show Timestamp In History.

# Autocomplete dirs only for `ls`, `lsd` and `exa`
compdef _dirs ls lsd exa

# - - - - - - - - - - - - - - - - - - - -
# ZI Configuration
# - - - - - - - - - - - - - - - - - - - -

if [[ ! -f $HOME/.zi/bin/zi.zsh ]]; then
  print -P "%F{33}▓▒░ %F{160}Installing (%F{33}z-shell/zi%F{160})…%f"
  command mkdir -p "$HOME/.zi" && command chmod go-rwX "$HOME/.zi"
  command git clone -q --depth=1 --branch "main" https://github.com/z-shell/zi "$HOME/.zi/bin" && \
    print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
    print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zi/bin/zi.zsh"
autoload -Uz _zi
(( ${+_comps} )) && _comps[zi]=_zi

# examples here -> https://wiki.zshell.dev/ecosystem/category/-annexes
zicompinit # <- https://wiki.zshell.dev/docs/guides/commands

zi light-mode for \
  z-shell/z-a-meta-plugins \
  @annexes # <- https://wiki.zshell.dev/ecosystem/category/-annexes

# examples here -> https://wiki.zshell.dev/community/gallery/collection
zicompinit # <- https://wiki.zshell.dev/docs/guides/commands


# - - - - - - - - - - - - - - - - - - - -
# Theme
# - - - - - - - - - - - - - - - - - - - -

ZSH_THEME="powerlevel10k"

zi snippet OMZL::git.zsh
zi snippet OMZP::git
zi cdclear -q

setopt prompt_subst

# Provide A Simple Prompt Till The Theme Loads
PS1="READY >"
zi ice wait'!' lucid
zi ice depth=1; zi light romkatv/powerlevel10k


# - - - - - - - - - - - - - - - - - - - -
# Plugins
# - - - - - - - - - - - - - - - - - - - -
zi is-snippet wait lucid for \
        atload"unalias grv g" \
    OMZP::{git,sudo,encode64,extract,safe-paste} \
    OMZP::{colorize,bgnotify,colored-man-pages,command-not-found} \
        if'[[ -d ~/.nvm ]]' \
    OMZP::nvm \
        if'[[ -d ~/.ssh ]]' \
    OMZP::ssh-agent \
        if'[[ -d ~/.gnupg ]]' \
    OMZP::gpg-agent \
        if'[[ "$OSTYPE" = *-gnu ]]' \
    OMZP::gnu-utils \
        has'pip' \
    OMZP::pip \
        has'python' \
    OMZP::python \
        has'tmux' \
    OMZP::tmux

#zi ice as"completion"
#zi snippet OMZP::docker/_docker

zi ice as"completion"
zi snippet OMZP::fd/_fd

zi ice as"completion"
zi snippet OMZP::ag/_ag

zi ice as"completion"
zi light esc/conda-zsh-completion

zi ice as"completion"
zi light zsh-users/zsh-completions

# - - - - - - - - - - - - - - - - - - - -
# User Configuration
# - - - - - - - - - - - - - - - - - - - -

bindkey '^[[3~' delete-char
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

bgnotify_threshold=300

setopt no_beep

# Local Config
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local

foreach piece (
    .completion.zsh
    .sources.zsh
    .exports.zsh
    .functions.zsh
    .aliases.zsh
) {
    source ~/$piece
}

[ -f ~/setupenv.sh ] && source ~/setupenv.sh


# - - - - - - - - - - - - - - - - - - - -
# cdr, persistent cd
# - - - - - - - - - - - - - - - - - - - -

# autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
# add-zsh-hook chpwd chpwd_recent_dirs
# DIRSTACKFILE="$HOME/.cache/zsh/dirs"
#
# # Make `DIRSTACKFILE` If It 'S Not There.
# if [[ ! -a $DIRSTACKFILE ]]; then
#     mkdir -p $DIRSTACKFILE[0,-5]
#     touch $DIRSTACKFILE
# fi
#
# if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
#     dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
# fi
#
# chpwd() {
#     print -l $PWD ${(u)dirstack} >>$DIRSTACKFILE
#     local d="$(sort -u $DIRSTACKFILE )"
#     echo "$d" > $DIRSTACKFILE
# }
#
# DIRSTACKSIZE=20
#
# setopt auto_pushd pushd_silent pushd_to_home
#
# setopt pushd_ignore_dups        # Remove Duplicate Entries
# setopt pushd_minus              # This Reverts The +/- Operators.


# - - - - - - - - - - - - - - - - - - - -
# Theme / Prompt Customization
# - - - - - - - - - - - - - - - - - - - -

# To Customize Prompt, Run `p10k configure` Or Edit `~/.p10k.zsh`.
[[ ! -f ~/.p10k.zsh ]] || . ~/.p10k.zsh


# - - - - - - - - - - - - - - - - - - - -
# End Profiling Script
# - - - - - - - - - - - - - - - - - - - -

if [[ "$PROFILE_STARTUP" == true ]]; then
    unsetopt xtrace
    exec 2>&3 3>&-
    zprof > ~/zshprofile
fi

if [[ "$TTY" = /dev/tty* ]]; then
    # adapted from Guake hybrid theme
    # https://github.com/ziyenano/Guake-Color-Schemes/blob/master/themes/hybrid.theme
    echo -en "\e]P0000000" #black
    echo -en "\e]P1A54242" #darkred
    echo -en "\e]P28C9440" #darkgreen
    echo -en "\e]P3de935f" #brown
    echo -en "\e]P45F819D" #darkblue
    echo -en "\e]P585678F" #darkmagenta
    echo -en "\e]P65E8D87" #darkcyan
    echo -en "\e]P7969896" #lightgrey
    echo -en "\e]P8373b41" #darkgrey
    echo -en "\e]P9cc6666" #red
    echo -en "\e]PAb5bd68" #green
    echo -en "\e]PBf0c674" #yellow
    echo -en "\e]PC81a2be" #blue
    echo -en "\e]PDb294bb" #magenta
    echo -en "\e]PE8abeb7" #cyan
    echo -en "\e]PFefefef" #white
    clear #for background artifacting
fi
zi light-mode for \
  z-shell/z-a-meta-plugins \
  @annexes # <- https://wiki.zshell.dev/ecosystem/category/-annexes
# examples here -> https://wiki.zshell.dev/community/gallery/collection
zicompinit # <- https://wiki.zshell.dev/docs/guides/commands

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
zi light-mode for \
  z-shell/z-a-meta-plugins \
  @annexes # <- https://wiki.zshell.dev/ecosystem/category/-annexes
# examples here -> https://wiki.zshell.dev/community/gallery/collection
zicompinit # <- https://wiki.zshell.dev/docs/guides/commands

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

zi light-mode for \
  z-shell/z-a-meta-plugins \
  @annexes # <- https://wiki.zshell.dev/ecosystem/category/-annexes
# examples here -> https://wiki.zshell.dev/community/gallery/collection
zicompinit # <- https://wiki.zshell.dev/docs/guides/commands
